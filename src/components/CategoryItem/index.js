import React from 'react';
import styled from 'styled-components'
import { keyframes } from 'styled-components'
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';

function CategoryItem({name, imageUrl='', path}) {
  let history = useHistory();

    return (
      <CategoryItemBox onClick={() => history.push('/menu/'+path)} imageUrl={imageUrl}>
        <div>
          {name}
        </div>
      </CategoryItemBox> 
    );
}

export default CategoryItem;

CategoryItem.propTypes = {
  name: PropTypes.string.isRequired,
  imageUrl: PropTypes.string,
};

const CategoryItemBox = styled.div`
  flex-basis: 33%;
  height: 263px;
  background-color: white;
  margin: 2px;
  background-image: ${props => `url(${props.imageUrl})`};
  background-repeat: no-repeat;
  background-size: contain;
  background-position: center;
  display:flex;
  justify-content: center;
  >div{
    align-self: flex-end;
    background-color: gold;
    height: 10%;
    padding: 15px 15px 5px;
    margin-bottom: 10px;
  }
`
