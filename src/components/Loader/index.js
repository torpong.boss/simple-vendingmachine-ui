import React from 'react';
// import PropTypes from 'prop-types';
import './index.css';

function Loader() {
  return (
    <div className="lds-ripple">
      <div />
      <div />
    </div>
  );
}

Loader.propTypes = {
};

export default Loader;
