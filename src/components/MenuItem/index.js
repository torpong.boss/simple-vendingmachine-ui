import React, { useState } from 'react';
import styled from 'styled-components'
import PropTypes from 'prop-types';
import axios from 'axios'


function MenuItem({available = 0, imageUrl='', price = 0, name=''}) {
  const [localAvailable, setLocalAvailable] = useState(available)
  const Buy = (name) => {
    console.log(name)
    axios.post(`http://127.0.0.1:8000/api/v1/products/buy`,{
      name: name,
      location: "Nonthaburi",
    })
    .then(res => {
      console.log(res)
      setLocalAvailable(res.data.quantity)
    })
  }

    return (
      <MenuItemBox imageUrl={imageUrl} onClick={() => Buy(name)}>
      <Tag>{price} ฿</Tag>
        <div className="item-label">
          Purchase
        </div>
        <div className="available-label">
          Available: {localAvailable}
        </div>
      </MenuItemBox> 
    );
}


export default MenuItem;

MenuItem.propTypes = {
  name: PropTypes.string.isRequired,
  imageUrl: PropTypes.string,
};

const MenuItemBox = styled.div`
  position: relative;
  flex-basis: 33%;
  height: 263px;
  background-color: white;
  margin: 2px;
  background-image: ${props => `url(${props.imageUrl})`};
  background-repeat: no-repeat;
  background-size: contain;
  background-position: center;
  display:flex;
  justify-content: center;
  >.available-label{
    border-radius: 10px;
    position: absolute;
    right: 10px;
    bottom: 10px;
    background-color: gold;
    height: 10%;
    padding: 15px 15px 5px;
    margin-bottom: 10px;
  }
  >.item-label{
    border-radius: 10px;
    position: absolute;
    right: 10px;
    bottom: 70px;
    background-color: gold;
    height: 10%;
    padding: 15px 15px 5px;
    margin-bottom: 10px;
  }
`
const Tag = styled.div`
	display: inline-block;
  position: absolute;
  right: 10px;
  top: 10px;
  width: auto;
	height: 38px;
	
	background-color: gold;
	-webkit-border-radius: 3px 4px 4px 3px;
	-moz-border-radius: 3px 4px 4px 3px;
	border-radius: 3px 4px 4px 3px;
	
	border-left: 1px solid gold;

	/* This makes room for the triangle */
	margin-left: 19px;
	
	font-weight: 300;
	font-family: 'Source Sans Pro', sans-serif;
	font-size: 22px;
	line-height: 38px;
  padding: 0 10px 0 10px;
  
  &:before{
    content: "";
    position: absolute;
    display: block;
    left: -19px;
    width: 0;
    height: 0;
    border-top: 19px solid transparent;
    border-bottom: 19px solid transparent;
    border-right: 19px solid gold;
  }
  &:after {
    content: "";
    background-color: white;
    border-radius: 50%;
    width: 4px;
    height: 4px;
    display: block;
    position: absolute;
    left: -9px;
    top: 17px;
  }
`
