import React, { useState, useEffect } from 'react';
import { Switch, Route } from 'react-router-dom';
// import { IntlProvider, FormattedMessage, FormattedNumber } from 'react-intl';
import { Home, Menu } from './screens';
import Layout from './layout'
import {
  Loader,
} from './components';
import { LocationProvider } from './context';
import Utils from './utils';
import './App.css';

function App() {
  // const [isLoading, setIsLoading] = useState(true);
  const [auth, setAuth] = useState(null);

  useEffect(() => {
    // rehydrate necessary hooks here before rendering main component
    // setTimeout(() => {
    //   setAuth(Utils.localStorage.getItem('auth'));
    //   setIsLoading(false);
    // }, 1500);
  }, []);

  // if (isLoading) {
  //   return <div className="loading-container"><Loader /></div>; // TODO: build loading component
  // }

  return (
    // <AuthProvider defaultValue={auth}>
    <LocationProvider defaultValue={auth}>
      <Switch>
        <Layout>
          <Route exact path="/" component={Home} />
          <Route path="/menu/:category" component={Menu} />
        </Layout>
      </Switch>
    </LocationProvider>
  );
}

export default App;
