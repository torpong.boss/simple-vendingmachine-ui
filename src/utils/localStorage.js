const setItem = (key, item) => {
  localStorage.setItem(key, JSON.stringify(item));
  return true;
};

const getItem = (key) => JSON.parse(localStorage.getItem(key));

export default {
  setItem,
  getItem,
};
