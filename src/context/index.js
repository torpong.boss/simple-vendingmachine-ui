import React, { useState, createContext, useEffect } from 'react';
import axios from 'axios';

export const LocationContext = createContext();

/* eslint react/prop-types: 0 */
export const LocationProvider = ({ children, defaultValue }) => {
  const [context, setContext] = useState(
    defaultValue
    || {
      location: "",
    },
  );

  useEffect(() => {
    axios.get(`http://127.0.0.1:8000/api/v1/get-locations`)
    .then(res => {
      setContext(res.data);
    })
  },[])

  const handler = (location) => {
    setContext({location: "temp"});
  };

  return (
    <LocationContext.Provider value={{ location: context, setLocation: handler }}>
      {children}
    </LocationContext.Provider>
  );
};
