// import React, { useContext } from 'react';
import React, { useState, useEffect, } from 'react';
import styled from 'styled-components'
import softData from './jsonData'
import { MenuItem } from '../../components'
import { useHistory, useParams } from 'react-router-dom';
import axios from 'axios'

function Menu() {
  // const { auth, setAuth } = useContext(AuthContext);
  let history = useHistory();
  let { category } = useParams();

  const [menuItems, setMenuItems] = useState([]);

  useEffect(() => {
    axios.post(`http://127.0.0.1:8000/api/v1/products/search/`,{
      category: category,
      location: "Nonthaburi",
    })
    .then(res => {
      console.log(res)
      setMenuItems(res.data);
    })
  },[])

  return (
    <MenuPage>
      <CategoryContainer>
        {menuItems.map(each => {
          return <MenuItem imageUrl={each.image_link}  available={each.quantity} price={each.price} name={each.name} />
          })
        }
      </CategoryContainer>
    </MenuPage>
  );
}



export default Menu;

const MenuPage = styled.div`
  display: block;
  background-color: gold;
  width: 100%;
  height: 100%;
  
`
const CategoryContainer = styled.div`
  display: flex;
  background-color: gold;
  width: 100%;
  height: 100%;
  flex-wrap: wrap;
  justify-content: flex-start;
  overflow: auto;
`

