const data = [
  {
    typeName: 'Energy Drink',
    color: 'silver',
    imageUrl: 'https://backend.tops.co.th/media//catalog/product/9/0/9002490221249.jpg',
    path:'energy_drink',
  },
  {
    typeName: 'Tea',
    color: 'blue',
    imageUrl: 'https://backend.tops.co.th/media//catalog/product/8/8/8858891300110_2_1.jpg',
    path:'tea',
  },
  {
    typeName: 'Soft Drink',
    color: 'green',
    imageUrl: 'https://backend.tops.co.th/media//catalog/product/8/8/8851959141014_e17-01-2019.jpg',
    path:'soft_drink',
  },
  {
    typeName: 'Plain Water',
    color: 'yellow',
    imageUrl: 'https://backend.tops.co.th/media//catalog/product/3/0/3068320114644_e19-06-2020.jpg',
    path:'plain_water',
  },
  {
    typeName: 'Juice',
    color: 'white',
    imageUrl: 'https://backend.tops.co.th/media//catalog/product/8/8/8853333013580_28042020.jpg',
    path:'juice',
  },
  {
    typeName: 'Coffee',
    color: 'purple',
    imageUrl: 'https://backend.tops.co.th/media/catalog/product/8/8/8850250000495_e1-12-2020.jpg?impolicy=resize&height=300&width=300',
    path:'coffee',
  },
  {
    typeName: 'Beer',
    color: 'orange',
    imageUrl: 'https://cdn.shopify.com/s/files/1/0066/2791/7914/products/NewProject-2020-04-24T104914.574_1024x1024.png?v=1588579344',
    path:'beer',
  },
  {
    typeName: 'Liquor',
    color: 'pink',
    imageUrl: 'https://cdn.shopify.com/s/files/1/0066/2791/7914/products/2698_480x.jpg?v=1560159740',
    path:'liquor',
  },
  {
    typeName: 'All',
    color: 'gold',
    imageUrl: 'https://backend.tops.co.th/media/catalog/product/8/8/8851717060090.jpg?impolicy=resize&height=300&width=300',
    path:'all',
  },
]

export default data;