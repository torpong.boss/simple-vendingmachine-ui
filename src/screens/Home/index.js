// import React, { useContext } from 'react';
import React, { useEffect, useState } from 'react';
import styled from 'styled-components'
import data from './jsonData'
import { CategoryItem } from '../../components'
import axios from 'axios'
import _ from 'lodash'

function Home() {

  const [category, setCategory] = useState([]);

  useEffect(() => {
    axios.get(`http://127.0.0.1:8000/api/v1/all-categories`)
    .then(res => {
      setCategory(res.data);
    })
  },[])

  return (
    <HomePage className="show">
      <CategoryContainer>
        {_.map(category, (each) => {
          return <CategoryItem key={each.name} name={each.name} path={each.name} imageUrl={each.image_link} />
          })
        }
      </CategoryContainer>
    </HomePage>
  );
}

export default Home;

const HomePage = styled.div`
  display: block;
  background-color: gold;
  width: 100%;
  height: 100%;

`
const CategoryContainer = styled.div`
  display: flex;
  background-color: gold;
  width: 100%;
  height: 100%;
  flex-wrap: wrap;
  justify-content: flex-start;
  overflow: auto;

`

