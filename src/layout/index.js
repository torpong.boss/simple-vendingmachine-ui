import React, { useState, useEffect, useContext } from 'react';
import styled from 'styled-components'
import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';
import data from './jsonData';
import { TiShoppingCart } from 'react-icons/ti';
import { IconContext } from 'react-icons';
import _ from 'lodash';
import axios from 'axios';
import { LocationContext } from '../context';


function Layout(props) {
  const { location } = useContext(LocationContext);

  const [localLocation, setLocalLocation] = useState([0]);

  useEffect(() => {
    setLocalLocation(_.map(location, 'location'))
  },[location])

    return (
      <LayoutContainer>
        <LayoutHeader>
          <DropdownContainer>
            <div className="mid-text">
              <p>Loction:</p>
            </div>
            <Dropdown options={localLocation} value={localLocation[0]} placeholder="Select an option" />
          </DropdownContainer>
          <HeaderDiv>
            <div className="mid-text">
              <p>Simple Vending Machine</p>
            </div>
          </HeaderDiv>
          <HeaderDiv className="IconContainer">
            <div >
              <IconContext.Provider value={{ size: "4em", className: "global-class-name" }}>
                <div style={{ alignSelf: 'center'}}>
                </div>
              </IconContext.Provider>
            </div>
          </HeaderDiv>
        </LayoutHeader>
        {props.children}
      </LayoutContainer>

    );
}

export default Layout;

const LayoutContainer = styled.div`
  height: calc(100% - 100px);
`
const LayoutHeader = styled.div`
  display: flex;
  height: 100px;
  background-color: orange;
  .mid-text{
    align-self: center;
    margin: 0 10px;
    >p{
      font-size: 1.5em;
      margin-block-start: 0.83em;
      margin-block-end: 0.83em;
      margin-inline-start: 0px;
      margin-inline-end: 0px;
      font-weight: bold;
      height:100%;
      margin: 0;
    }
  }
  .IconContainer{
    display: flex;
    justify-content: flex-end;
    margin-right: 10px;
    >div{
      display: flex;
    }
  }
`
const HeaderDiv = styled.div`
  display:flex;
  height: 100%;
  flex: 1;
  justify-content: center;

`
const DropdownContainer = styled.div`
  display: flex;
  height: 100%;
  flex: 1;
  .Dropdown-root{
    align-self: center;
  }
 
`


